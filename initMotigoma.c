#include <stdio.h>

#include "matrix.h"

static char *pieceNames[] = {"G", "L", "E", "C", "C", "E", "L", "G"};
static char *directionNames[] = {"△", "▽"};

void initMotigoma(board *bp) {
    for (int i = 0; i < N; i++) {
        bp->motigomap[i] = NULL;
    }
}

void initZahyou(board *bp) {
    for (int i = 0; i < xmax; i++) {
        for (int j = 0; j < ymax; j++) {
            bp->zahyoup[i][j] = NULL;
        }
    }
}

void initBoard(board *bp) {
    koma initKomas[N] = {
        {GOTE, 0, 0},  {GOTE, 1, 0},  {GOTE, 2, 0},  {GOTE, 1, 1},
        {SENTE, 1, 2}, {SENTE, 0, 3}, {SENTE, 1, 3}, {SENTE, 2, 3},
    };
    koma *ikp = initKomas;
    koma *kp = bp->komas;

    initMotigoma(bp);
    initZahyou(bp);

    for (int i = 0; i < N; i++) {
        bp->zahyoup[ikp->y][ikp->x] = kp;
        *kp++ = *ikp++;
    }
}

void display(board *bp) {
    printf("Player ▽(後手)の持ち駒:");
    for (int i = 0; i < N; i++) {
        if (bp->motigomap[i] != NULL) {
            if (bp->motigomap[i]->player == GOTE) {
                printf("%s,", pieceNames[bp->motigomap[i] - bp->komas]);
            }
        }
    }

    printf("\n 後手:%d\n 先手:%d\n \n", GOTE, SENTE);
    printf("1\t 2\t 3\n");
    for (int i = 0; i < ymax; i++) {
        printf("%d", i);
        for (int j = 0; j < xmax; j++) {
            if (bp->zahyoup[i][j] == NULL) {
                printf("|.....");
            } else {
                printf("|%s(%s)", pieceNames[bp->zahyoup[i][j] - bp->komas],
                       directionNames[bp->zahyoup[i][j]->player]);
            }
        }
        printf("|\n");
    }
    printf("\nPlayer △(先手)の持ち駒:");
    for (int i = 0; i < N; i++) {
        if (bp->motigomap[i] != NULL) {
            if (bp->motigomap[i]->player == SENTE) {
                printf("%s,", pieceNames[bp->motigomap[i] - bp->komas]);
            }
        }
    }
}

/*
static char *pieceNames[] = {"L", "E", "G", "C", "H"};
static char *directionNames[MAX_PLAYERS + 1] = {"▽", "", "△"};

Player ▽(産技花子)の持ち駒:
後手；-1
先手；1
   1    2    3
0|G(▽)|L(▽)|E(▽)|
1|.....|C(▽)|.....|
2|.....|C(△)|.....|
3|E(△)|L(△)|G(△)|

Player △(高専太郎)の持ち駒:

条件？ ：『|G(△)』をプリント;

if (pnum == NULL)

*/