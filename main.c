#include <stdio.h>

#include "matrix.h"

int main() {
    board brd;

    initBoard(&brd);
    display(&brd);
    return 0;
}