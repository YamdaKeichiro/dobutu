#ifndef __MATRIX_H
#define __MATRIX_H
#define xmax 3
#define ymax 4
#define N 8


typedef struct {
    int player;
    int x;
    int y;
} koma;

typedef struct {
    koma komas[N];
    koma *zahyoup[ymax][xmax];
    koma *motigomap[N];
} board;

typedef enum { G_A, L_A, E_A, C_A, C_B, E_B, L_B, G_B } pieceType;

typedef enum { SENTE, GOTE } playerTurn;

void initMotigoma(board *bp);
void initZahyou(board *bp);
void initKomas(board *bp);
void initBoard(board *bp);
void display(board *bp);


/*
static char *pieceNames[] = {"L", "E", "G", "C", "H"};
static char *directionNames[MAX_PLAYERS + 1] = {"▽", "", "△"};
static const char pref[] = "Tokyo"; staticグローバル変数(配列:読取専用)
*/
#endif